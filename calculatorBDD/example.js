function add(a,b){
	return a+b;	
}

function sub(a,b){
	return a-b;	
}

function mult(a,b){
	return a*b;	
}

function div(a,b){	
	return a/b;	
}

function fact(n)
{
    if (n < 0) {
        return NaN;
    }
	
	if (n == 0) {
        return 1;
    }
  
	if(n>=1){
		var result = 1;
		for(i=1; i<=n ; i++) {
			result *= i;
		}
		return result;
	}
}