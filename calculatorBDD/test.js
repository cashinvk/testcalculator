﻿//тестирование сложения двух чисел
describe("add", function(){	
	describe("Складывает два целых числа a и  b", function(){
			
	function makeTest(a,b){
		var expected = a+b;;
		it("при сложении "+ a + " и "+ b+ " результат: "+expected,function(){
			assert.equal(add(a,b),expected);
		});			
	}		
		for(var a=-10; a<=3;a+=3.5){			
			for(var b=10; b>=-10;b-=3.5){			
				makeTest(a,b);
				add(a,b);			
			}
		}	
	});   
}); 

//тестирование разности двух чисел
describe("sub", function(){	
	describe("Разность двух чисел a и  b", function(){	
				
	function makeTest(a,b){
		var expected = a-b;;
		it("при разности "+ a + " и "+ b+ " результат: "+expected,function(){
			assert.equal(sub(a,b),expected);
		});			
	}			
		for(var a=-10; a<=3;a+=3.5){			
			for(var b=10; b>=-10;b-=3.5){			
				makeTest(a,b);
				sub(a,b);			
			}
		}
	});   
}); 

//тестирование произведения двух чисел
describe("mult", function(){	
	describe("Произведение двух чисел a и b", function(){				
	
	function makeTest(a,b){
		var expected = a*b;;
		it("при умножении "+ a + " и "+ b+ " результат: "+expected,function(){
			assert.equal(mult(a,b),expected);
		});			
	}		
		for(var a=-10; a<=3;a+=3.5){			
			for(var b=10; b>=-10;b-=3.5){			
				makeTest(a,b);
				mult(a,b);			
			}
		}
	});   
});
//тестирование деления двух чисел
describe("div", function(){		
	describe("Деление двух чисел a и b", function(){		
		function makeTest(a,b){
		var expected = a/b;
		if(a!=0){	
		it("при делении "+ a + " и "+ b+ " результат: "+expected.toFixed(2),function(){
			assert.equal(div(a,b),expected);
		});		}	
	}		
		for(a=-5; a<=5;a++){			
			for(b=5; b>=-5;b--){			
				makeTest(a,b);
				div(a,b);			
			}
		}		
		it("при делении 0 на 0 результат: NaN",function(){
			assert(isNaN(div(0,0)));
		});		
	});   
});
////тестирование факториала числа
describe("fact", function(){	
	describe("Факториао числа", function(){								
		it("Факториал отрицательного числа результат: NaN",function(){
			assert(isNaN(fact(-1)));
		});	
		it("Факториал числа 0 результат: 1",function(){
			assert.equal(fact(0),1);
		});	
		it("Факториал числа 1 результат: 1",function(){
			assert.equal(fact(1),1);
		});		
		it("Факториал числа 2 результат: 2",function(){
			assert.equal(fact(2),2);
		});
		it("Факториал числа 3 результат: 6",function(){
			assert.equal(fact(3),6);
		});
		it("Факториал числа 4 результат: 24",function(){
			assert.equal(fact(4),24);
		});
		it("Факториал числа 5 результат: 120",function(){
			assert.equal(fact(5),120);
		});
			
		for(var n=-1; n>=5;n++){			
				fact(n);							
			}				
	});   
});